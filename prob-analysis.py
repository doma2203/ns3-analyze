import pandas as pd
import scipy.stats as st
import sys
import numpy as np
import os.path
import glob
import matplotlib
import decimal
import matplotlib.pyplot as plt
matplotlib.style.use('seaborn-whitegrid')

index=sorted(glob.glob(sys.argv[1]+"/prob*.txt"),key=os.path.getmtime)
# index2=sorted(glob.glob(sys.argv[1]+"/nano_nodes_*[0-9].txt"),key=os.path.getmtime)
print(index)

prob=[pd.read_csv(i, delimiter=',')['avg_succ_frames'] for i in index]

values = [i.mean() for i in prob]
stddevs = [i.std() for i in prob]
conf=[1.960*i/np.sqrt(5) for i in stddevs]

# print(values)
datas=pd.DataFrame({'probability':np.arange(.1,1.,.1,),'avg_succ_frames':values})
print(datas)

plot=datas.plot(kind='line',x='probability',y='avg_succ_frames',legend=False,yerr=conf,ylim=(0.3,1),yticks=np.arange(0.0,1,0.05))
# plot.legend(bbox_to_anchor=(0., .923, 1., .102),loc='upper right',fontsize='small')
plot.set(title="Impact of probability on successful frames ratio",xlabel="Probability", ylabel="Successful frames ratio")
plt.savefig('result_plots/succ_frames_vs_probability.png')
# plt.show()