import pandas as pd
import scipy.stats as st
import sys
import numpy as np
import os.path
import glob
import matplotlib
import decimal
import matplotlib.pyplot as plt
matplotlib.style.use('seaborn-whitegrid')

index=sorted(glob.glob(sys.argv[1]+"/modulation*.txt"),key=os.path.getmtime)
# index2=sorted(glob.glob(sys.argv[1]+"/nano_nodes_*[0-9].txt"),key=os.path.getmtime)
print(index)

prob=[pd.read_csv(i, delimiter=',')['JFI'] for i in index]

values = [i.mean() for i in prob]
stddevs = [i.std() for i in prob]
conf=[1.960*i/np.sqrt(5) for i in stddevs]

# print(values)
datas=pd.DataFrame({'modulation':[1,2,3,4,5,6],'JFI':values})
print(datas)

plot=datas.plot(kind='bar',x='modulation',y='JFI',legend=False,yerr=conf,ylim=(0.95,1),yticks=np.arange(0.9,1,0.025))
# plot.legend(bbox_to_anchor=(0., .923, 1., .102),loc='upper right',fontsize='small')
plot.set(title="Impact of modulation on JFI",xlabel="Modulation method", ylabel="Jain's fairness index")
plt.savefig('result_plots/modulation_vs_jfi.png')
# plt.show()