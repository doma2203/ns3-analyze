import pandas as pd
import scipy.stats as st
import sys
import numpy as np
import os.path
import glob
import matplotlib
import matplotlib.pyplot as plt
matplotlib.style.use('seaborn-whitegrid')

index=sorted(glob.glob(sys.argv[1]+"/nano*_aware.txt"),key=os.path.getmtime)
index2=sorted(glob.glob(sys.argv[1]+"/nano_nodes_*[0-9].txt"),key=os.path.getmtime)


aware=[pd.read_csv(i, delimiter=',')[' avg_succ_frames'] for i in index]
non_aware=[pd.read_csv(i, delimiter=',')[' avg_succ_frames'] for i in index2]
aware_values = [i.mean() for i in aware]
aware_stddevs = [i.std() for i in aware]
non_aware_values = [i.mean() for i in non_aware]
non_aware_stddevs = [i.std() for i in non_aware]
print(non_aware_values)
print(aware_values)
datas=pd.DataFrame({'host':[np.float(os.path.basename(i).split('_')[2]) for i in index],'Traffic aware amplitude addresing: on':aware_values,'Traffic aware amplitude addresing: off':non_aware_values})
print(datas['Traffic aware amplitude addresing: on'])

plot=datas.plot(kind='bar',x='host',grid=0.05,xticks=range(20),yticks=np.arange(0.95,1.,step=0.005),ylim=(0.95,1.))
plot.legend(bbox_to_anchor=(0., .923, 1., .102),loc='upper right',fontsize='small')
plot.set(title="Jain's fairness index",xlabel="Number of hosts", ylabel="JFI")
# plt.savefig('result_plots/jfi_vs_hosts.png')
# plt.show()
# plot_x.set(xlabel="host", ylabel="Decoding rate")